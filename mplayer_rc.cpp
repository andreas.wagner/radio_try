#include <stdio.h>
#include <string.h>

FILE *mplayer_pipe = NULL;

void mplayer_close()
{
	if(mplayer_pipe != NULL)
	{
		fputs("q", mplayer_pipe);
		pclose(mplayer_pipe);
	}
}

void mplayer_vol_up()
{
	if(mplayer_pipe != NULL)
	{
		fputs("*", mplayer_pipe);
	}
}
void mplayer_vol_down()
{
	if(mplayer_pipe != NULL)
	{
		fputs("/", mplayer_pipe);
	}
}

void mplayer_open(char *radio_stream)
{
	char cmd[9000] = "/usr/bin/mplayer -nolirc ";
	strcat(cmd, radio_stream);
	
	fprintf(stderr, "opening mplayer\n");
	
	mplayer_pipe = popen(cmd, "w");
	
	fprintf(stderr, "setting buffering\n");
	
	setvbuf(mplayer_pipe, NULL, _IONBF, 0);
	
	fprintf(stderr, "sending empty string\n");
	
	fputs("", mplayer_pipe);
}
