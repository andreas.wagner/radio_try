#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>

#include "radiolist.h"

#define FAIL_PERROR {char buf[5000] = "";\
	sprintf(buf, "%s, %s line %d:",__FILE__, __func__, __LINE__);\
	perror(buf);}
enum read_state
{
	reading_name,
	reading_link
};


radioentry entries[100];

int number_of_entries;


void *file_ptr;
int fd;
struct stat st;

void read_radios(char *filename)
{
	number_of_entries = 0;
	fd = open(filename, O_RDONLY);
	if(fd == -1)
	{
		FAIL_PERROR
		exit(EXIT_FAILURE);
	}
	fstat(fd, &st);
	file_ptr = mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
	close(fd);
	if(file_ptr == NULL)
	{
		FAIL_PERROR
		exit(EXIT_FAILURE);
	}
	int entry_no = 0;
	read_state state = reading_name;
	void *tmp = file_ptr;
	for(void *i = file_ptr; i < file_ptr+st.st_size; i++)
	{
		printf("%c", *((char*)i));
		if( (*((char*)(i)) == '\t') && (state == reading_name) )
		{
			*((char*)(i)) = '\0';
			entries[entry_no].name = (char*) tmp;
			state = reading_link;
			tmp = i+1;
		}
		else if( *((char *)i) == '\n' && (state == reading_link))
		{
			*((char*)(i)) = '\0';
			entries[entry_no].link = (char*) tmp;
			state = reading_name;
			tmp = i+1;
			number_of_entries++;
			entry_no++;
		}
	}
}

void free_radios()
{
	munmap(file_ptr, st.st_size);
}
