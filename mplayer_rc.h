#ifndef _MPLAYER_RC_H
#define _MPLAYER_RC_H

void mplayer_close();
void mplayer_open(char *radio_stream);
void mplayer_vol_up();
void mplayer_vol_down();

#endif
