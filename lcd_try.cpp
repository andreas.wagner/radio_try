#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <time.h> //?

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/select.h>
#include <ctype.h>
#include <signal.h>


#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringSerial.h>
#include <lcd.h>

#define FAIL_PERROR {char buf[5000] = "";\
	sprintf(buf, "%s, %s line %d:",__FILE__, __func__, __LINE__);\
	perror(buf);}


#define LCD_ROW 2
#define LCD_COL 16
#define LCD_BUS 4
#define LCD_UPDATE_PERIOD 300

#define BUFSIZE 1024

static unsigned char lcdFb[LCD_ROW][LCD_COL];
static int lcdHandle = 0;

char *fifo_path = "lcd_rc";
int fifo_fd = -1;
volatile bool exiting = false;

char buffer[BUFSIZE];
fd_set fifo_fd_set;
char input[BUFSIZE] = "";

#define PORT_LCD_RS 7
#define PORT_LCD_E 0
#define PORT_LCD_D4 2
#define PORT_LCD_D5 3
#define PORT_LCD_D6 1
#define PORT_LCD_D7 4

#define PORT_BUTTON1 5
#define PORT_BUTTON2 6

const int ledPorts[] =
{
	21,
	22,
	23,
	24,
	11,
	26,
	27
};

#define MAX_LED_COUNT sizeof(ledPorts) / sizeof(ledPorts[0])

int lcd_init()
{
	lcdHandle = lcdInit(LCD_ROW, LCD_COL, LCD_BUS,
		PORT_LCD_RS, PORT_LCD_E,
		PORT_LCD_D4, PORT_LCD_D5, PORT_LCD_D6, PORT_LCD_D7,
		0, 0, 0, 0);
	if(lcdHandle < 0)
	{
		fprintf(stderr, "%s, Line %d: lcdInit() failed.", __FILE__, __LINE__);
		return -1;
	}
	
	for(int i = 0; i < MAX_LED_COUNT; i++)
	{
		pinMode(ledPorts[i], OUTPUT);
		pullUpDnControl(ledPorts[i], PUD_OFF);
		digitalWrite(ledPorts[i], 1);
	}
	pinMode(PORT_BUTTON1, INPUT);
	pullUpDnControl(PORT_BUTTON1, PUD_UP);
	pinMode(PORT_BUTTON2, INPUT);
	pullUpDnControl(PORT_BUTTON2, PUD_UP);
	return 0;
}


void exit_func()
{
	exiting = true;
	close(fifo_fd);
	remove(fifo_path);
}

void sigint_func(int)
{
	exit_func();
}

void scroll()
{
	memcpy(lcdFb[0], lcdFb[1], sizeof(lcdFb[0]));
	memset(lcdFb[1], ' ', sizeof(lcdFb[1]));
}

/*
void lcd_pause()
{
	struct timeval t =
	{
		.tv_sec = 0,
		.tv_usec = 10000
	};
	select(0, NULL, NULL, NULL, &t);
}
*/
int main(int argc, char *argv[])
{
	wiringPiSetup();
	if(lcd_init() < 0)
	{
		return 1;
	}
	for(int row = 0; row < LCD_ROW; row++)
	{
		lcdPosition(lcdHandle, 0, row);
		for(int col = 0; col < LCD_COL; col++)
		{
			lcdPutchar(lcdHandle, ' ');
			lcdFb[row][col] = ' ';
		}
	}
	int ret = 0;
	int old_umask = umask(~(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));
	ret = mkfifo(fifo_path, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if(ret == -1)
	{
		FAIL_PERROR
		exit(EXIT_FAILURE);
	}
	umask(old_umask);
	
	fifo_fd = open(fifo_path, O_RDONLY | O_NONBLOCK);
	FD_SET(fifo_fd, &fifo_fd_set);
	
	int flags = fcntl(fifo_fd, F_GETFL);
	int result = fcntl(fifo_fd, F_SETFL, flags | O_NONBLOCK);
	
	atexit(&exit_func);
	signal(SIGINT, &sigint_func);
	
	int pos = 0;
	while(true)
	{
		int s_ret = 0;
		FD_ZERO(&fifo_fd_set);
		FD_SET(fifo_fd, &fifo_fd_set);
		bool retry = true;
		while(retry)
		{
			s_ret = select(fifo_fd+1, &fifo_fd_set, NULL, NULL, NULL);
			if(s_ret == -1)
			{
				if(errno != EINTR)
				{
					if(exiting)
					{
						exit(EXIT_SUCCESS);
					}
					FAIL_PERROR
					exit(EXIT_FAILURE);
				}
				else
				{
					retry = true;
				}
			}
			else
			{
				retry = false;
			}
		}
		
		ssize_t bytes_read = 0;
		bool cont_read = true;
		ssize_t input_pos = 0;
		while(cont_read && FD_ISSET(fifo_fd, &fifo_fd_set))
		{
			bytes_read = read(fifo_fd, (void *)&buffer, BUFSIZE-1);
			
			if(bytes_read == -1)
			{
				if(errno != EWOULDBLOCK)
				{
					FAIL_PERROR
					exit(EXIT_FAILURE);
				}
				else
				{
					cont_read = false;
					sched_yield();
				}
			}
			else // bytes_read != -1
			{
				if(bytes_read == 0)
				{
					cont_read = false;
					close(fifo_fd);
					fifo_fd = open(fifo_path, O_RDONLY | O_NONBLOCK);
				}
				else
				{
					buffer[bytes_read+1] = '\0';
					input_pos += strlen(buffer);
					if(input_pos > BUFSIZE-1) // ignore beginning of too long lines
					{
						input[0] = '\0';
						input_pos = 0;
					}
					strcat(input, buffer);
					input[input_pos+1] = '\0';
				}
			}
		}
		if(input[0] != '\0')
		{
			printf("%s\n", input);
			char *end = input + strlen(input);
			for(char *i = input; i < end; i++)
			{
				if(isalpha(*i)||isdigit(*i)||(*i=='.')
					||(*i==',')||(*i==';')||(*i==':')
					||(*i=='-')||(*i=='_')||(*i==' ')
					||(*i=='*')||(*i=='+')||(*i=='~')
					||(*i=='#')||(*i=='"')||(*i=='\''))
				{
					lcdFb[1][pos] = *i;
					pos++;
					if(pos >= LCD_COL)
					{
						scroll();
						pos = 0;
					}
				}
				else if(*i == '\n')
				{
					scroll();
					pos = 0;
				}
				else if(*i == '\r')
				{
					pos = 0;
				}
			}
			for(int row = 0; row < LCD_ROW; row++)
			{
				lcdPosition(lcdHandle, 0, row);
				//lcd_pause();
				for(int col = 0; col < LCD_COL; col++)
				{
					lcdPutchar(lcdHandle, lcdFb[row][col]);
					//lcd_pause();
				}
				
			}
			input[0] = '\0';
			input_pos = 0;
		}
	}
	
	return 0;
}
