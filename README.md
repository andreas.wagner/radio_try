# radio_try

This repo consists of two parts:
* "mplayer-radio", which reads from a named pipe and remote-controls mplayer accordingly. Use irexec (from lircd) to echo strings to the named pipe.
* "lcd_server", which reads from a named pipe and puts the strings recieved onto an ODROID 16x2-LCD-Shield.