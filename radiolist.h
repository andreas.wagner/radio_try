#ifndef _RADIOLIST_H
#define _RADIOLIST_H

struct radioentry
{
	char *name;
	char *link;
};

extern radioentry entries[100];

extern int number_of_entries;

void read_radios(char *filename);
void free_radios();

#endif
