#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <sys/select.h>
#include <ctype.h>
#include <signal.h>

#include "radiolist.h"
#include "mplayer_rc.h"

#define FAIL_PERROR {char buf[5000] = "";\
	sprintf(buf, "%s, %s line %d:",__FILE__, __func__, __LINE__);\
	perror(buf);}


#define BUFSIZE 1024

char buffer[BUFSIZE];
char *fifo_path = "radio_rc";
int fifo_fd = -1;

char *lcd_fifo_path = "lcd_rc";

fd_set fifo_fd_set;

volatile int current_radio_index = 0;

volatile int lastdigit = 0;

void alerthandler(int)
{
	char mybuf[2] = "\0";
	int my_new = 0;
	mybuf[0] = lastdigit;
	lastdigit = '\0';
	sscanf(mybuf, "%d", &my_new);
	if(!(my_new < 0 || my_new > number_of_entries))
	{
		current_radio_index = my_new;
		mplayer_close();
		printf("%s\n", entries[current_radio_index].name);
		mplayer_open(entries[current_radio_index].link);
	}
}

void cleanup()
{
	if(fifo_fd > -1)
	{
		close(fifo_fd);
	}
	remove(fifo_path);
	mplayer_close();
}

void play_index(int index)
{
	printf("%s\n", entries[index].name);
	mplayer_open(entries[index].link);
	FILE *file = fopen(lcd_fifo_path, "a");
	if(file != NULL)
	{
		//setvbuf(file, NULL, _IONBF, 0);
		fprintf(file, "\n%s\n", entries[index].name);
		//fflush(file);
		fclose(file);
	}
}

int main(int argc, char *argv[])
{
	FD_ZERO(&fifo_fd_set);
	char input[BUFSIZE] = "";
		
	bool go_on = true;
	atexit(cleanup);
	int ret = 0;
	ret = mkfifo(fifo_path, S_IRUSR | S_IWUSR);
	if(ret == -1)
	{
		FAIL_PERROR
		exit(EXIT_FAILURE);
	}
	
	fprintf(stderr, "Opening FIFO\n");
	
	fifo_fd = open(fifo_path, O_RDONLY | O_NONBLOCK);
	
	if(fifo_fd == -1)
	{
		FAIL_PERROR
		exit(EXIT_FAILURE);
	}
	FD_SET(fifo_fd, &fifo_fd_set);
	
	fprintf(stderr, "Setting nonblocking\n");
	
	int flags = fcntl(fifo_fd, F_GETFL);
	int result = fcntl(fifo_fd, F_SETFL, flags | O_NONBLOCK);
	
	fprintf(stderr, "reading radiostations.txt\n");
	read_radios("radiostations.txt");
	
	for(int i = 0; i < number_of_entries; i++)
	{
		printf("read %s\n", entries[i].name);
	}
	
	printf("setting up signal handler\n");
	
	signal(SIGALRM, alerthandler);
	
	play_index(0);
	
	
	while(go_on)
	{
		ssize_t bytes_read = 0;
		bool cont_read = true;
		ssize_t input_pos = 0;
		
		int s_ret = 0;
		
		//printf("select\n");
		FD_ZERO(&fifo_fd_set);
		FD_SET(fifo_fd, &fifo_fd_set);
		bool retry = true;
		while(retry)
		{
			s_ret = select(fifo_fd+1, &fifo_fd_set, NULL, NULL, NULL);
			if(s_ret == -1)
			{
				if(errno != EINTR)
				{
					FAIL_PERROR
					exit(EXIT_FAILURE);
				}
				else
				{
					retry = true;
				}
			}
			else
			{
				retry = false;
			}
			
		}
		
		//printf("loop; %d\n", s_ret);
		while(cont_read && FD_ISSET(fifo_fd, &fifo_fd_set))
		{
			bytes_read = read(fifo_fd, (void *)&buffer, BUFSIZE-1);
			//printf("bytes read: %ld\n", bytes_read);
			
			if(bytes_read == -1)
			{
				if(errno != EWOULDBLOCK)
				{
					FAIL_PERROR
					exit(EXIT_FAILURE);
				}
				else
				{
					cont_read = false;
					sched_yield();
				}
			}
			else // bytes_read != -1
			{
				if(bytes_read == 0)
				{
					cont_read = false;
					close(fifo_fd);
					fifo_fd = open(fifo_path, O_RDONLY | O_NONBLOCK);
				}
				else
				{
					buffer[bytes_read+1] = '\0';
					input_pos += strlen(buffer);
					input[input_pos+1] = '\0';
					if(input_pos > BUFSIZE-1) // ignore beginning of too long lines
					{
						input[0] = '\0';
						input_pos = 0;
					}
					strcat(input, buffer);
				}
			}
		}
		if(input[0] != '\0')
		{
			if(isdigit(input[0]))
			{
				if(lastdigit != '\0')
				{
					alarm(0);
					char mybuf[3] = "\0\0";
					int my_new = 0;
					mybuf[1] = input[0];
					mybuf[0] = lastdigit;
					lastdigit = '\0';
					sscanf(mybuf, "%d", &my_new);
					if(!(my_new < 0 || my_new > number_of_entries))
					{
						current_radio_index = my_new;
						mplayer_close();
						play_index(current_radio_index);
					}
				}
				else
				{
					lastdigit = input[0];
					alarm(2);
				}
			}
			else if(strstr(input, "quit") !=NULL)
			{
				go_on = false;
			}
			else if(strstr(input, "vol_plus") !=NULL)
			{
				mplayer_vol_up();
			}
			else if(strstr(input, "vol_minus") !=NULL)
			{
				mplayer_vol_down();
			}
			else if(strstr(input, "next") !=NULL)
			{
					current_radio_index++;
					current_radio_index %= number_of_entries;
					mplayer_close();
					play_index(current_radio_index);
			}
			else if(strstr(input, "prev") !=NULL)
			{
					current_radio_index--;
					if(current_radio_index < 0)
					{
						current_radio_index = number_of_entries-1;
					}
					mplayer_close();
					play_index(current_radio_index);
			}
			else
			{
				printf("'%s' discarded\n", input);
			}
		}
		input[0] = '\0';
		input_pos = 0;
	}
}
